return {
  {
    "williamboman/mason.nvim",
    lazy = false,
    config = function()
      require("mason").setup()
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    lazy = false,
    config = function()
      require("mason-lspconfig").setup({
        ensure_installed = { "lua_ls", "bashls", "eslint", "pyright" },
        auto_install = true,
      })
    end,
  },
  {
    "neovim/nvim-lspconfig",
    lazy = false,
    opts = {
      servers = {
        terraformls = {},
      },
    },
    config = function()
      local lspconfig = require("lspconfig")
      local coq = require("coq") -- add this

      --Setup lspservers:
      lspconfig.ts_ls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.lua_ls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.pyright.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.terraformls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.tflint.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.groovyls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.docker_compose_language_service.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.dockerls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.csharp_ls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.csharp_ls.setup(coq.lsp_ensure_capabilities({}))

      lspconfig.html.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.angularls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.bashls.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.eslint.setup(coq.lsp_ensure_capabilities({}))
      lspconfig.jsonls.setup(coq.lsp_ensure_capabilities({}))

      -- Keymaps
      vim.keymap.set("n", "<leader>gD", vim.lsp.buf.declaration, {})
      vim.keymap.set("n", "<leader>gd", vim.lsp.buf.definition, {})
      vim.keymap.set("n", "K", vim.lsp.buf.hover, {})
      vim.keymap.set("n", "<leader>ca", vim.lsp.buf.code_action, {})
    end,
  },
}
