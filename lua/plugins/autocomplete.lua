return {
  {
    "ms-jpq/coq_nvim",
    config = function()
      vim.g.coq_settings = {
        auto_start = true,
      }
      vim.cmd("COQnow --shut-up")
    end,
  },
  { "ms-jpq/coq_nvim",       branch = "coq" },
  { "ms-jpq/coq.artifacts",  branch = "artifacts" },
  { "ms-jpq/coq_nvim",       branch = "coq" },
  { "ms-jpq/coq.thirdparty", branch = "3p" },
  {
    'Exafunction/codeium.vim',
    event = 'BufEnter',
    config = function()
      vim.keymap.set('i', '<C-g>', function() return vim.fn['codeium#Accept']() end, { expr = true, silent = true })
    end
  },
}

