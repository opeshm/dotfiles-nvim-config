return {
  {
    "akinsho/toggleterm.nvim",
    config = function()
      require("toggleterm").setup()
      -- Terminal
      vim.keymap.set("n", "<leader>tt", ":ToggleTerm direction=float<CR>")
      vim.keymap.set("t", "<esc>", "<C-\\><C-n>")
    end,
  },
}
