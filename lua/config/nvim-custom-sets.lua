vim.opt.hidden = true
vim.opt.backup = false
vim.opt.swapfile = false
vim.opt.nu = true
vim.opt.rnu = true
vim.opt.wrap = false

vim.opt.errorbells = false
vim.opt.colorcolumn = "100"

vim.opt.laststatus = 2
vim.opt.wildmenu = true

vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.tabstop = 2

vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.hlsearch = true

vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- set termguicolors to enable highlight groups
vim.opt.termguicolors = true

local autocmd = vim.api.nvim_create_autocmd
autocmd('BufEnter', {
  pattern = '*',
  command = 'silent! loadview'
})
autocmd('BufEnter', {
  pattern = '*',
  command = 'lua showColorColumn()'
})
autocmd('BufEnter', {
  pattern = 'Jenkinsfile',
  command = 'set syntax=groovy'
})
