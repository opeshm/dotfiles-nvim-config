-- KeyMaps for buildIn NeoVim actions
-- File
vim.keymap.set("n", "<leader>fs", ":w<CR>:mkview<CR>")
vim.keymap.set("n", "<leader>wq", ":q<CR>")
vim.keymap.set("n", "<leader>bd", ":bd<CR>")

-- Windows
vim.keymap.set("n", "<leader>wh", "<C-W>h")
vim.keymap.set("n", "<leader>wj", "<C-W>j")
vim.keymap.set("n", "<leader>wk", "<C-W>k")
vim.keymap.set("n", "<leader>wl", "<C-W>l")
vim.keymap.set("n", "<leader>w|", ":vsplit<CR>")
vim.keymap.set("n", "<leader>w-", ":split<CR>")
vim.keymap.set("n", "<leader>md", ":set mouse=<CR>")
vim.keymap.set("n", "<leader>me", ":set mouse=a<CR>")

-- Coding
vim.keymap.set('v', '<leader>fc', ':fo<CR>')
