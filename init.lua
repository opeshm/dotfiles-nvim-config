-- Make sure to set `mapleader` before lazy so your mappings are correct
require("config.nvim-custom-sets")
require("config.nvim-custom-functions")
require("config.nvim-keymap")

-- Lazy
require("config.lazy")

